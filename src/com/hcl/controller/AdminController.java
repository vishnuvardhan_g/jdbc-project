package com.hcl.controller;

import java.util.List;

import com.hcl.dao.IAdminDao;
import com.hcl.dao.IAdminImpl;
import com.hcl.model.Admin;
import com.hcl.model.Players;

public class AdminController {
  int result;
  IAdminDao dao=new IAdminImpl();
  
  public int adminAuthentication(String uid,String password) {
    Admin admin=new Admin(uid,password);
    return dao.adminAuthentication(admin);
  }
  public List<Players> viewAllPlayers() {
    return dao.viewAllPlayers();  
  }
  public int addPlayer(int playerid,String pname,String country) {
    Players info=new Players(playerid,pname,country);
    return dao.addPlayer(info);
  }
  public int editPname(int playerid,String pname) {
    Players info=new Players();
    info.setPlayerid(playerid);
    info.setPname(pname);
    return dao.editPname(info);
    
  }
  public int editCountry(int playerid,String country) {
    Players info=new Players();
    info.setPlayerid(playerid);
    info.setCountry(country);
    return dao.editCountry(info);
    
  }
  public int editPnameCountry(int playerid,String pname,String country) {
    Players info=new Players();
    info.setPlayerid(playerid);
    info.setPname(pname);
    info.setCountry(country);
    return dao.editPnameCountry(info);
      }
  public int deletePlayer(int playerid) {
    Players info=new Players();
    info.setPlayerid(playerid);
    
    return dao.deletePlayer(info);
    
  }
}
