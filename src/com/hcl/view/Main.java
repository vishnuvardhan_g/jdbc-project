package com.hcl.view;


import java.util.List;
import java.util.Scanner;

import com.hcl.controller.AdminController;
import com.hcl.model.Players;

public class Main {

  public static void main(String[] args) {
    
    Scanner sc= new Scanner(System.in);
    System.out.println("Enter franchise name");
    String uid=sc.nextLine();
    System.out.println("Enter password");
    String password=sc.nextLine();
    AdminController controller=new AdminController();
    int result=0;
    result=controller.adminAuthentication(uid,password);
    if (result>0) {
      System.out.println("Hi "+uid+", welcome to franchise page");
      int cont=0;
      do {
      System.out.println("1.Add Player 2.Remove player 3.Edit player 4.View all Players");
      int option=sc.nextInt();
      int playerid=0;
      String pname="";
      String country="";
      switch(option) {
      case 1:
        System.out.println("Enter Player ID");
        playerid=sc.nextInt();
        sc.nextLine();
        System.out.println("Enter Player Name");
        pname=sc.nextLine();
        System.out.println("Enter Player Country");
        country = sc.nextLine();
        controller.addPlayer(playerid, pname, country);
        if(result>0) {
          System.out.println(playerid+" added successfully");
        }
        break;
      case 2:
        System.out.println("Enter Player Id to remove from DB");
        playerid=sc.nextInt();
        result=controller.deletePlayer(playerid);
        System.out.println((result>0)?playerid+"Removed record Successfully":"Remove failed");
        break;
      case 3:
        System.out.println("1.Edit player Name 2.Edit Player country 3.Edit Player Name and country");
        option=sc.nextInt();
        if(option==1) {
          System.out.println("Enter the Player Id");
          playerid=sc.nextInt();
          sc.nextLine();
          System.out.println("Enter the player Name");
          pname=sc.nextLine();
          result=controller.editPname(playerid, pname);
          System.out.println((result>0)?playerid+"Updated Successfully":"Update failed");
        }else if(option==2) {
          System.out.println("Enter the Player Id");
          playerid=sc.nextInt();
          sc.nextLine();
          System.out.println("Enter the player Country");
          country=sc.nextLine();
          result=controller.editCountry(playerid, country);
          System.out.println((result>0)?playerid+"Updated Successfully":"Update failed");
          
        }else if(option==3) {
          System.out.println("Enter the Player Id");
          playerid=sc.nextInt();
          sc.nextLine();
          System.out.println("Enter the player Name");
          pname=sc.nextLine();
          System.out.println("Enter the player Country");
          country=sc.nextLine();
          result=controller.editPnameCountry(playerid,pname, country);
          System.out.println((result>0)?playerid+"Updated Successfully":"Update failed");
          
        }else {
          System.out.println("Invalid Edit option");
        }
        break;
      case 4:
        List<Players> list=controller.viewAllPlayers();
        if(list.size()>0) {
          System.out.println("playerid,pname,country");
          for (Players p:list) {
            System.out.println(p.getPlayerid()+","+p.getPname()+","+p.getCountry());
            
          }
        }else {
          System.out.println("No Records found");
        }
        break;
       default:
        System.out.println("Invalid Selection");
      }
      System.out.println("Would you like to continue press 1");
      cont = sc.nextInt();
      }while(cont == 1);
      }
      else {
      System.out.println("Franchise ID or password is incorrect");
    }
    System.out.println("Application executed succefully");
    sc.close();

  }

  }   

