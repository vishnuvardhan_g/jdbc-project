package com.hcl.model;

public class Players {
  private Integer playerid;
  private String pname;
  private String country;
public Players() {
  //TODO Auto-generated constructor stub
}
public Players(Integer playerid, String pname, String country) {
  super();
  this.playerid = playerid;
  this.pname = pname;
  this.country = country;
}
public Integer getPlayerid() {
  return playerid;
}
public void setPlayerid(Integer playerid) {
  this.playerid = playerid;
}
public String getPname() {
  return pname;
}
public void setPname(String pname) {
  this.pname = pname;
}
public String getCountry() {
  return country;
}
public void setCountry(String country) {
  this.country = country;
}

  

}
