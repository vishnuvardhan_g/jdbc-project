package com.hcl.util;

public class Query {
  public static String adminAuth="select * from admin where uid=? and password=?";
  public static String viewAll="select * from players";
  public static String addPlayer="insert into players value(?,?,?)";
  public static String editPname="update players set pname=? where playerid=?";
  public static String editCountry="update players set country=? where playerid=?";
  public static String editPnameCountry="update players set pname=?,country=? where playerid=?";
  public static String deletePlayer="delete from players where playerid=?";


}
