package com.hcl.dao;
import com.hcl.model.Admin;
import com.hcl.model.Players;
import java.util.List;

public interface IAdminDao {
  public int adminAuthentication(Admin admin);
  public List<Players> viewAllPlayers();
  public int addPlayer(Players info);
  public int editPname(Players info);
  public int editCountry(Players info);
  public int editPnameCountry(Players info);
  public int deletePlayer (Players info);

}
