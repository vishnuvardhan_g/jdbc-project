package com.hcl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hcl.model.Admin;
import com.hcl.model.Players;
import com.hcl.util.Db;
import com.hcl.util.Query;


public class IAdminImpl implements IAdminDao {
  PreparedStatement pst;
  ResultSet rs;
  int result;
  
  @Override
  public int adminAuthentication(Admin admin) {
    result=0;
    
    try {
      pst=Db.getConnection().prepareStatement(Query.adminAuth);
      pst.setString(1, admin.getUid());
      pst.setString(2, admin.getPassword());
      rs=pst.executeQuery();
      while(rs.next()) {
        result++;
      }
    } catch (ClassNotFoundException | SQLException e) {
      System.out.println(" Exception occurs in Admin Authentication");
  }finally {
    try {
      pst.close();
      rs.close();
      Db.getConnection().close();
    } catch (SQLException | ClassNotFoundException e) {
    
    }
   
  }
    return result;

  }
  @Override
  public List<Players> viewAllPlayers() {
    List<Players> list=new ArrayList<Players>();
    try {
      pst=Db.getConnection().prepareStatement(Query.viewAll);
      rs=pst.executeQuery();
      while (rs.next()) {
        Players info= new Players(rs.getInt(1),rs.getString(2),rs.getString(3));
        list.add(info);   
      }
    } catch (ClassNotFoundException | SQLException e) {
      System.out.println("Exception occured at view all Players");
    } finally {
      try {
        Db.getConnection().close();
        rs.close();
        pst.close();
      } catch (ClassNotFoundException | SQLException e) {
        
      }
      
    }
    
    return list;
  }
  @Override
  public int addPlayer(Players info) {
    result=0;
    try {
      pst=Db.getConnection().prepareStatement(Query.addPlayer);
      pst.setInt(1, info.getPlayerid());
      pst.setString(2, info.getPname());
      pst.setString(3, info.getCountry());
      result= pst.executeUpdate();
      
    } catch (ClassNotFoundException | SQLException e) {
      System.out.println("Exception occured at add Players");
    }finally {
      try {
        Db.getConnection().close();
        pst.close();
      }catch(ClassNotFoundException | SQLException e) {
        
      }
    }
    return result;
  }
  @Override
  public int editPname(Players info) {
    result=0;
    try {
      pst=Db.getConnection().prepareStatement(Query.editPname);
      pst.setString(1, info.getPname());
      pst.setInt(2, info.getPlayerid());
      result=pst.executeUpdate();
    } catch (ClassNotFoundException | SQLException e) {
      System.out.println("Exception while editing Player Name");
    }finally {
      try {
        Db.getConnection().close();
        pst.close();
      } catch (ClassNotFoundException | SQLException e) {
        
      }
    }
    return result;
  }
  @Override
  public int editCountry(Players info) {
    result=0;
    try {
      pst=Db.getConnection().prepareStatement(Query.editCountry);
      pst.setString(1, info.getCountry());
      pst.setInt(2, info.getPlayerid());
      result=pst.executeUpdate();
    } catch (ClassNotFoundException | SQLException e) {
      System.out.println("Exception while editing Player Country");
    }finally {
      try {
        Db.getConnection().close();
        pst.close();
      } catch (ClassNotFoundException | SQLException e) {
        
      }
    }
    return result;
  }
  @Override
  public int editPnameCountry(Players info) {
    result=0;
    try {
      pst=Db.getConnection().prepareStatement(Query.editPnameCountry);
      pst.setString(1, info.getPname());
      pst.setString(2, info.getCountry());
      pst.setInt(3, info.getPlayerid());
      result=pst.executeUpdate();
    } catch (ClassNotFoundException | SQLException e) {
      System.out.println("Exception while editing Player Name and Country");
    }finally {
      try {
        Db.getConnection().close();
        pst.close();
      } catch (ClassNotFoundException | SQLException e) {
        
      }
    }
    return result;
  }
  @Override
  public int deletePlayer(Players info) {
    result=0;
    try {
      pst=Db.getConnection().prepareStatement(Query.deletePlayer);
      pst.setInt(1, info.getPlayerid());
      result=pst.executeUpdate();
    } catch (ClassNotFoundException | SQLException e) {
      System.out.println("Exception while removing Player");
    }finally {
      try {
        Db.getConnection().close();
        pst.close();
      } catch (ClassNotFoundException | SQLException e) {
        
      }
    }
    return result;
  }
}
